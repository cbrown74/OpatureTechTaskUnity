﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRenderer : MonoBehaviour {

	Corners corners;
	LineRenderer lineRenderer;
	public Color c1 = Color.yellow;
	public Color c2 = Color.red;
	public Color complete = Color.green;

	// Use this for initialization
	void Start () {
		corners = GetComponent<Corners>();
		lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		lineRenderer.startWidth = 5f;	

		// A simple 2 color gradient with a fixed alpha of 1.0f.
		float alpha = 1.0f;
		Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(c1, 0.0f), new GradientColorKey(c2, 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
		);
		lineRenderer.colorGradient = gradient;
	}
	
	// Update is called once per frame
	void Update () {
		lineRenderer.positionCount = corners.GetComponent<Corners> ().getVector2Vertices().Length;
		lineRenderer.SetPositions (corners.GetComponent<Corners> ().getVector3Vertices ());
	
		if (corners.isComplete) {
			c1 = Color.green;
			c2 = Color.green;
			lineRenderer.SetColors (c1, c2);
		}
	}
}
