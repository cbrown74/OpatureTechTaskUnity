﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curso : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Cursor.visible = false;

	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = new Vector3 ((Input.mousePosition.x - Screen.width / 2), (Input.mousePosition.y - Screen.height / 2));
		transform.position = pos;
	}
}
