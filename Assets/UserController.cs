﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserController : MonoBehaviour {
	Color red = Color.red;
	Color green = Color.green;
	int lengthOfLineRenderer = 2;
	int currentLineRenderer = 0;
	bool noCorners= true;
	public Corners corners;
	LineRenderer lineRenderer;
	float x, y;
	Vector2 pz;
	Vector2 firstCornerPos;
	bool drawing = true;
	float distance = 0f;

	LineIntersectionDetector intersectionDetector = new LineIntersectionDetector();

	string drawMessage = "";
	void Start()
	{
		Cursor.visible = false;
		updateMousePosition ();
		lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		lineRenderer.startWidth = 5f;	
		lineRenderer.positionCount = lengthOfLineRenderer;
		lineRenderer.SetPosition (0, new Vector3 (x, y, 1));
	}

	void OnGUI() {
		if( drawing)
			GUI.Label(new Rect(Input.mousePosition.x, -Input.mousePosition.y + Screen.height + 10, 100, 20), drawMessage);
	}

	void updateMousePosition(){
		x = (Input.mousePosition.x - Screen.width / 2);
		x = x - (x % 10);
		y = (Input.mousePosition.y - Screen.height / 2);
		y = y - (y % 10);
	}

	void Update () {
		if (drawing) {
			draw ();
		} 
	}

	void draw(){
		distanceFromLastCorner ();

		if (noCorners) {
			drawMessage = "Click to start wall";
			lineRenderer.enabled = false;
		} else {
			drawMessage = distance + " CM";
			lineRenderer.enabled = true;
		}

		bool safeToDraw = !doesThisLineIntersectAnyWalls ();



		updateMousePosition ();
		lineRenderer.SetPosition (1, new Vector3 (x, y, 1));

		if (safeToDraw) {
			lineRenderer.SetColors (green, green);
			if (Input.GetMouseButtonDown (0)) {
				if (noCorners) {
					firstCornerPos = new Vector2 (x, y);
					noCorners = false;
				}

				lineRenderer.SetPosition (0, new Vector3 (x, y, 1));
				corners.addCorner (x, y);
				pz = new Vector2 (x, y);

			}
		} else if (firstCornerPos.Equals (new Vector2 (x, y))) {
			corners.addCorner (x, y);
			corners.complete ();
			lineRenderer.enabled = false;
			drawing = false;

		} else {
			drawMessage = "ILLEGAL";
			//lineRenderer.SetColors (red, red);
			lineRenderer.material.SetColor("_TintColor", Color.red);
		}
	}

	public void distanceFromLastCorner(){
			 distance =  Vector2.Distance (pz, new Vector2 (x, y));
	}


	bool doesThisLineIntersectAnyWalls(){

		Vector2[] tempVert = corners.getVector2Vertices ();
		Vector2 tempMousePos = new Vector2 (x, y);
		for (int i = 0; i < tempVert.Length-2; i++) {

			if(intersectionDetector.LineSegmentIntersection(tempVert[i], tempVert[i+1], pz, tempMousePos)){
				return true;
			}

		}

		return false;

	}




}
