﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corners : MonoBehaviour {


	//public Stack<Vector2> corners = new Stack<Vector2>();
	//Originally used stack but list traversal easier for editing existing walls
	public List<Vector2> corners = new List<Vector2>();
	Vector3[] cornersv3;

	float x, y;

	public bool isComplete = false;
	public bool firstCornerFlag = true;

	static float distance = 0;


	void Start(){
		populateVector3Array ();
	}

	public void addCorner(float x, float y){
		corners.Add (new Vector2(x,y));
		populateVector3Array ();
	}



	public void complete(){
		this.isComplete = true;
	}

	public float getDrawLineDistance(){
		return distance;
	}


	public void populateVector3Array(){

			cornersv3 = new Vector3[corners.ToArray ().Length];

			for (int i = 0; i < corners.ToArray ().Length; i++) {
				cornersv3 [i].Set (corners.ToArray () [i].x, corners.ToArray () [i].y, 1);
			}
		
		} 


	public Vector2[] getVector2Vertices(){
		return corners.ToArray ();
	}
	public Vector3[] getVector3Vertices(){

		return cornersv3;
	}
}
